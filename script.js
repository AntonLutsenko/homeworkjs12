let currentImg = 0;
let previousImg = 0;
let interval = setInterval(showImg, 3000);
const arr = Array.from(document.getElementsByClassName("image-to-show"));
const btnStop = document.querySelector(".btnStop");
const btnStart = document.querySelector(".btnStart");
btnStop.addEventListener("click", stopBtnClick);
btnStart.addEventListener("click", startBtnClick);

let count = () => {
	currentImg++;
	previousImg = currentImg - 1;
	if (currentImg === arr.length) {
		currentImg = 0;
	}
	return currentImg;
};

function showImg() {
	count();
	arr[currentImg].classList.remove("hidden-img");
	if (currentImg === 0) {
		arr[previousImg].classList.add("hidden-img");
	} else {
		arr[currentImg - 1].classList.add("hidden-img");
	}
}

function stopBtnClick() {
	clearTimeout(interval);
	btnStop.setAttribute("disabled", "disabled");
	document.querySelector(".btnStart").removeAttribute("disabled");
}
function startBtnClick() {
	interval = setInterval(showImg, 3000);
	btnStart.setAttribute("disabled", "disabled");
	document.querySelector(".btnStop").removeAttribute("disabled");
}
